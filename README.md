## Back-End

An Enterprise grade E-Commecerce application utilizing Java Spring Data REST and React JS

Please run the server portion of the application after configuring the database in order for the front-end to load it's content


## Available Scripts

In the app directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

The application utilizes rest APIs to serve and be served it's content

### 'Developed by Malindra Perera'