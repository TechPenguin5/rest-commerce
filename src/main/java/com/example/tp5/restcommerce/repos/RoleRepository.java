package com.example.tp5.restcommerce.repos;

import com.example.tp5.restcommerce.entities.Role;
import com.example.tp5.restcommerce.entities.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;


// AUTO IMPLEMENTED
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(RoleName roleName);
}