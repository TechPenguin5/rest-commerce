package com.example.tp5.restcommerce.repos;

import com.example.tp5.restcommerce.entities.Tag;
import org.springframework.data.jpa.repository.JpaRepository;


// AUTO IMPLEMENTED
public interface TagRepository extends JpaRepository<Tag, Integer> {

}