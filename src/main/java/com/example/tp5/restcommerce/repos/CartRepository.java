package com.example.tp5.restcommerce.repos;

import com.example.tp5.restcommerce.entities.Cart;
import com.example.tp5.restcommerce.entities.Product;
import com.example.tp5.restcommerce.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface CartRepository extends JpaRepository<Cart, Integer> {
    Cart findByUserId(int userId);
}