package com.example.tp5.restcommerce.repos;

import com.example.tp5.restcommerce.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;


public interface ProductRepository extends JpaRepository<Product, Integer> {

    List<Product> findByTagsName(String name);

}