package com.example.tp5.restcommerce.repos;

import com.example.tp5.restcommerce.entities.Wishlist;
import org.springframework.data.jpa.repository.JpaRepository;


// AUTO IMPLEMENTED
public interface WishlistRepository extends JpaRepository<Wishlist, Integer> {

}