package com.example.tp5.restcommerce.repos;

import com.example.tp5.restcommerce.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;


// AUTO IMPLEMENTED
public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
}