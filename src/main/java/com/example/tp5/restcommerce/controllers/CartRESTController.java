package com.example.tp5.restcommerce.controllers;

import com.example.tp5.restcommerce.entities.Cart;
import com.example.tp5.restcommerce.entities.Product;
import com.example.tp5.restcommerce.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/cart")
//@PreAuthorize("hasRole('ROLE_USER')")
public class CartRESTController {

    @Autowired
    private CartService service;

    @PostMapping
    public Cart createCart(@RequestBody Product product) {
        return service.saveProduct(product, 0);
    }

    @GetMapping
    public List<Product> getAllCart() {
        return service.getAllCartProduct();
    }

    @DeleteMapping("/{id}")
    public void disableUser(@PathVariable("id") int id) {
        service.removeProduct(id);
    }
}
