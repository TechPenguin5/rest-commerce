package com.example.tp5.restcommerce.controllers;

import com.example.tp5.restcommerce.entities.Wishlist;
import com.example.tp5.restcommerce.entities.Product;
import com.example.tp5.restcommerce.services.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/wishlist")
public class WishlistRESTController {

    @Autowired
    private WishlistService service;

    @PostMapping
    public Wishlist createWishlist(@RequestBody Product product) {
        return service.saveProduct(product);
    }

    @PutMapping
    public Wishlist updateWishlist(@RequestBody Product product) {
        return service.saveProduct(product);
    }

    @GetMapping
    public List<Product> getAllWishlist() {
        return service.getAllWishlistProduct();
    }

    @DeleteMapping("/{id}")
    public void disableUser(@PathVariable("id") int id) {
        service.removeProduct(id);
    }
}
