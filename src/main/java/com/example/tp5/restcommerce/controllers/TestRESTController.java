package com.example.tp5.restcommerce.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestRESTController {

    @GetMapping("/test/user")
    public String userAccess() {
        return "User access granted!";
    }

    @GetMapping("/test/admin")
    public String adminAccess() {
        return "Admin access granted!";
    }
}