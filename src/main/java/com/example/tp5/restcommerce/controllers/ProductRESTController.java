package com.example.tp5.restcommerce.controllers;

import com.example.tp5.restcommerce.entities.Product;
import com.example.tp5.restcommerce.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/products")
public class ProductRESTController {

    @Autowired
    private ProductService service;


    @GetMapping
    public List<Product> getProducts(
            @RequestParam(value="category", required=false) String category,
            @RequestParam(value="search", required=false) String search) {
                return service.getProducts(category, search);
    }

    @GetMapping("/{id}")
    public Product getProduct(@PathVariable("id") int id) {
        return service.getProductById(id);
    }

    @PostMapping
    public Product createProduct(@RequestBody Product product) {
        return service.saveProduct(product);
    }

    @PutMapping
    public Product updateProduct(@RequestBody Product product) {
        return service.saveProduct(product);
    }

    @DeleteMapping("/{id}")
    public void disableUser(@PathVariable("id") int id) {
        service.disableProduct(id);
    }
}
