package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.Cart;
import com.example.tp5.restcommerce.entities.Product;
import com.example.tp5.restcommerce.entities.User;

import java.util.List;


public interface CartService {

    void removeProduct(int id);

    Cart saveProduct(Product product, int userId);

    List<Product> getAllCartProduct();
}
