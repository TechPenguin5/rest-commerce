package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.Country;
import java.util.List;


public interface CountryService {
    Country saveCountry(Country country);

    Country updateCountry(Country country);

    void disableCountry(int id);

    Country getCountryById(int id);

    List<Country> getAllCountries();
}
