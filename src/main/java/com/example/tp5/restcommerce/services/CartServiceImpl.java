package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.Cart;
import com.example.tp5.restcommerce.entities.Product;
import com.example.tp5.restcommerce.entities.User;
import com.example.tp5.restcommerce.repos.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Service
public class CartServiceImpl implements CartService{


    @Autowired
    private CartRepository repository;

    @Override
    public Cart saveProduct(Product product, int userId) {

        Cart cart = new Cart();

//        cart = repository.findByUserId(userId);

        cart.getProducts().add(product);

        return repository.save(cart);
    }


    @Override
    public void removeProduct(int id) {

        List<Cart> cartList = repository.findAll();

        for (Cart cart: cartList) {
            for (Product prod : cart.getProducts()) {
                if (prod.getId() == id) {
                    repository.delete(cart);
                }
            }
        }
    }

    @Override
    public List<Product> getAllCartProduct() {

        List<Cart> cartList = repository.findAll();

        Collection<Product> cartProducts = new ArrayList<Product>();

        for (Cart cart: cartList) {
            for (Product prod : cart.getProducts()) {
                cartProducts.add(prod);
            }
        }

        return new ArrayList<Product>(cartProducts);
    }
}
