package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.Wishlist;
import com.example.tp5.restcommerce.entities.Product;
import com.example.tp5.restcommerce.repos.WishlistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Service
public class WishlistServiceImpl implements WishlistService{

    @Autowired
    private WishlistRepository repository;

    @Override
    public Wishlist saveProduct(Product product) {

        Wishlist wishlist = new Wishlist();
        wishlist.getProducts().add(product);

        return repository.save(wishlist);
    }

    @Override
    public Wishlist updateWishlist(Wishlist wishlist) {
        return repository.save(wishlist);
    }

    @Override
    public void removeProduct(int id) {

        List<Wishlist> wishlistList = repository.findAll();

        for (Wishlist wishlist: wishlistList) {
            for (Product prod : wishlist.getProducts()) {
                if (prod.getId() == id) {
                    repository.delete(wishlist);
                }
            }
        }
    }

    @Override
    public List<Product> getAllWishlistProduct() {

        List<Wishlist> wishlistList = repository.findAll();

        Collection<Product> wishlistProducts = new ArrayList<Product>();

        for (Wishlist wishlist: wishlistList) {
            for (Product prod : wishlist.getProducts()) {
                wishlistProducts.add(prod);
            }
        }

        return new ArrayList<Product>(wishlistProducts);
    }
}
