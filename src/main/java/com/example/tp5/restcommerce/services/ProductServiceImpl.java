package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.Product;
import com.example.tp5.restcommerce.repos.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;


@Service
public class ProductServiceImpl implements ProductService{


    @Autowired
    private ProductRepository repository;


    @Override
    public Product saveProduct(Product product) {
        return repository.save(product);
    }


    @Override
    public Product updateProduct(Product product) {
        return repository.save(product);
    }


    @Override
    public Product getProductById(int id) {
        return repository.findById(id).orElse(null);
    }


    @Override
    public void disableProduct(int id) {
        Product product = repository.findById(id).orElse(null);
        product.setAvailable(false);
        repository.save(product);
    }


    @Override
    public List<Product> getProducts(String category, String search) {

        if ((category=="" || category==null) && (search=="" || search==null)) {

            return repository.findAll();

        }
        else if ((category=="" || category==null) && search!="") {

            List<Product> products = repository.findAll();
            List<Product> filterProducts = new ArrayList<>();

            for (Product product: products) {
                if (product.getTitle().toLowerCase().contains(search.toLowerCase())) {
                    filterProducts.add(product);
                }
            }

            return filterProducts;
        }
        else if (category!="" && (search=="" || search==null)) {

            return repository.findByTagsName(category);

        }
        else {

            List<Product> categoryProducts = repository.findByTagsName(category);
            List<Product> filterProducts = new ArrayList<>();

            for (Product product: categoryProducts) {
                if (product.getTitle().toLowerCase().contains(search.toLowerCase())) {
                    filterProducts.add(product);
                }
            }

            return filterProducts;
        }
    }
}
