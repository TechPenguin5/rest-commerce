package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.Brand;
import java.util.List;


public interface BrandService {
    Brand saveBrand(Brand brand);

    Brand updateBrand(Brand brand);

    void disableBrand(int id);

    Brand getBrandById(int id);

    List<Brand> getAllBrands();
}
