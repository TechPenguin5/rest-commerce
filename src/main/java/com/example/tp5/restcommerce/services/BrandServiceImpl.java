package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.Brand;
import com.example.tp5.restcommerce.repos.BrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class BrandServiceImpl implements BrandService{

    @Autowired
    private BrandRepository repository;

    @Override
    public Brand saveBrand(Brand brand) {
        return repository.save(brand);
    }

    @Override
    public Brand updateBrand(Brand brand) {
        return repository.save(brand);
    }

    @Override
    public void disableBrand(int id) {
    }

    @Override
    public Brand getBrandById(int id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<Brand> getAllBrands() {
        return repository.findAll();
    }
}
