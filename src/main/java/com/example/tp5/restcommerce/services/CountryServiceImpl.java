package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.Country;
import com.example.tp5.restcommerce.repos.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class CountryServiceImpl implements CountryService{

    @Autowired
    private CountryRepository repository;

    @Override
    public Country saveCountry(Country country) {
        return repository.save(country);
    }

    @Override
    public Country updateCountry(Country country) {
        return repository.save(country);
    }

    @Override
    public void disableCountry(int id) {
    }

    @Override
    public Country getCountryById(int id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<Country> getAllCountries() {
        return repository.findAll();
    }
}
