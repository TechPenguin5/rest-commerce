package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.Tag;
import com.example.tp5.restcommerce.repos.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class TagServiceImpl implements TagService{

    @Autowired
    private TagRepository repository;

    @Override
    public Tag saveTag(Tag tag) {
        return repository.save(tag);
    }

    @Override
    public Tag updateTag(Tag tag) {
        return repository.save(tag);
    }

    @Override
    public void disableTag(int id) {
    }

    @Override
    public Tag getTagById(int id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<Tag> getAllTags() {
        return repository.findAll();
    }
}
