package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.Wishlist;
import com.example.tp5.restcommerce.entities.Product;
import java.util.List;


public interface WishlistService {
    Wishlist updateWishlist(Wishlist wishlist);

    void removeProduct(int id);

    Wishlist saveProduct(Product product);

    List<Product> getAllWishlistProduct();
}
