package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.Tag;
import java.util.List;


public interface TagService {
    Tag saveTag(Tag tag);

    Tag updateTag(Tag tag);

    void disableTag(int id);

    Tag getTagById(int id);

    List<Tag> getAllTags();
}
