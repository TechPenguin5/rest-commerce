package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.Product;
import java.util.List;


public interface ProductService {
    Product saveProduct(Product product);

    Product updateProduct(Product product);

    void disableProduct(int id);

    Product getProductById(int id);

    List<Product> getProducts(String category, String search);
}
