package com.example.tp5.restcommerce.services;

import com.example.tp5.restcommerce.entities.User;
import java.util.List;


public interface UserService {

    User saveUser(User user);

    User updateUser(User user);

    void disableUser(int id);

    User getUserById(int id);

    List<User> getAllUsers();
}
