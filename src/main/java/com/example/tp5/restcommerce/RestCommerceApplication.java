package com.example.tp5.restcommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class RestCommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestCommerceApplication.class, args);
	}

}