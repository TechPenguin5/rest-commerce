package com.example.tp5.restcommerce.util;

import com.example.tp5.restcommerce.entities.*;
import com.example.tp5.restcommerce.repos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;


@Component
public class DataInitializer implements CommandLineRunner {

    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final BrandRepository brandRepository;
    private final TagRepository tagRepository;
    private final CountryRepository countryRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public DataInitializer(UserRepository userRepository, ProductRepository productRepository, BrandRepository brandRepository, TagRepository tagRepository, CountryRepository countryRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.brandRepository = brandRepository;
        this.tagRepository = tagRepository;
        this.countryRepository = countryRepository;
        this.roleRepository = roleRepository;
    }

    //Used to preload data into the database
    @Override
    public void run(String... strings) throws Exception {


        //Generate Roles
        this.roleRepository.save(new Role(
                RoleName.ROLE_USER
        ));

        Role admin = new Role(RoleName.ROLE_ADMIN);

        this.roleRepository.save(admin);

        Set<Role> adminRole = new HashSet<>();

        adminRole.add(admin);



        //Generate Default User
        User adminUser = new User(
                "admin",
                "admin",
                "admin",
                "admin@admin.com",
                "pass123"
        );

        adminUser.setRoles(adminRole);

        this.userRepository.save(adminUser);



        //Generate and save Default Brands
        Brand ralph_lauren = brandRepository.save(new Brand("Ralph Lauren"));
        Brand lous_vuitton = brandRepository.save(new Brand("Louis Vuitton"));
        Brand belvest = brandRepository.save(new Brand("Belvest"));
        Brand burberry = brandRepository.save(new Brand("Burberry"));


        //Generate Default Tags
        Tag sale = tagRepository.save(new Tag("Sale"));
        Tag summer_collection = tagRepository.save(new Tag("Summer Collection"));
        Tag men = tagRepository.save(new Tag("Men"));
        Tag women = tagRepository.save(new Tag("Women"));


        //Generate Default Countries
        Country usa = countryRepository.save(new Country("USA"));
        Country uk = countryRepository.save(new Country("UK"));
        Country russia = countryRepository.save(new Country("Russia"));
        Country india = countryRepository.save(new Country("India"));
        Country taiwan = countryRepository.save(new Country("Taiwan"));


        //Generate Default Products
        Product product1 = new Product();
        product1.setTitle("White Top");
        product1.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        product1.setPrice(BigDecimal.valueOf(24));
        product1.setColor("White");
        product1.setImage("images/product/product-02.jpg");
        product1.setQuantity(30);
        product1.setAvailable(true);
        product1.setBrand(ralph_lauren);

        product1.getCountries().add(usa);
        product1.getCountries().add(uk);

        product1.getTags().add(sale);
        product1.getTags().add(summer_collection);
        product1.getTags().add(women);

        this.productRepository.save(product1);


        Product product2 = new Product();
        product2.setTitle("Blue Shirt");
        product2.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        product2.setPrice(BigDecimal.valueOf(30));
        product2.setColor("White");
        product2.setImage("images/product/product-03.jpg");
        product2.setQuantity(10);
        product2.setAvailable(true);
        product2.setBrand(belvest);

        product2.getCountries().add(uk);
        product2.getCountries().add(russia);

        product2.getTags().add(sale);
        product2.getTags().add(men);

        this.productRepository.save(product2);


        Product product3 = new Product();
        product3.setTitle("Leather Coat");
        product3.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        product3.setPrice(BigDecimal.valueOf(30));
        product3.setColor("Brown");
        product3.setImage("images/product/product-04.jpg");
        product3.setQuantity(6);
        product3.setAvailable(true);
        product3.setBrand(lous_vuitton);

        product3.getCountries().add(uk);
        product3.getCountries().add(russia);

        product3.getTags().add(sale);
        product3.getTags().add(women);

        this.productRepository.save(product3);


        Product product4 = new Product();
        product4.setTitle("Grey Top");
        product4.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        product4.setPrice(BigDecimal.valueOf(15));
        product4.setColor("Brown");
        product4.setImage("images/product/product-05.jpg");
        product4.setQuantity(3);
        product4.setAvailable(true);
        product4.setBrand(lous_vuitton);

        product4.getCountries().add(uk);
        product4.getCountries().add(russia);

        product4.getTags().add(sale);
        product4.getTags().add(women);

        this.productRepository.save(product4);


        Product product5 = new Product();
        product5.setTitle("Leather Belt");
        product5.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing eli..");
        product5.setPrice(BigDecimal.valueOf(30));
        product5.setColor("Brown");
        product5.setImage("images/product/product-12.jpg");
        product5.setQuantity(15);
        product5.setAvailable(true);
        product5.setBrand(burberry);

        product5.getCountries().add(taiwan);
        product5.getCountries().add(india);

        product5.getTags().add(sale);
        product5.getTags().add(men);

        this.productRepository.save(product5);


        Product product6 = new Product();
        product6.setTitle("Sneakers");
        product6.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        product6.setPrice(BigDecimal.valueOf(15));
        product6.setColor("Black");
        product6.setImage("images/product/product-09.jpg");
        product6.setQuantity(9);
        product6.setAvailable(true);
        product6.setBrand(lous_vuitton);

        product6.getCountries().add(uk);
        product6.getCountries().add(russia);

        product5.getTags().add(sale);
        product6.getTags().add(men);

        this.productRepository.save(product6);


        Product product7 = new Product();
        product7.setTitle("Blue Smart Shirt");
        product7.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        product7.setPrice(BigDecimal.valueOf(25));
        product7.setColor("Blue");
        product7.setImage("images/product/product-11.jpg");
        product7.setQuantity(13);
        product7.setAvailable(true);
        product7.setBrand(lous_vuitton);

        product7.getCountries().add(uk);
        product7.getCountries().add(russia);

        product7.getTags().add(sale);
        product7.getTags().add(men);

        this.productRepository.save(product7);

    }
}



