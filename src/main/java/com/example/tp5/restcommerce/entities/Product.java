package com.example.tp5.restcommerce.entities;

import lombok.Data;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;


@Data
@Entity
@Table(name="PRODUCT")
public class Product {

    private @Id
    @GeneratedValue
    Integer id;

    private String title;
    private String description;
    private BigDecimal price;
    private String color;
    private String image;
    private int quantity;
    private boolean available;

    @ManyToOne
    private Brand brand;

    @ManyToMany
    private Collection<Country> countries = new ArrayList<Country>();

    @ManyToMany
    private Collection<Tag> tags = new ArrayList<Tag>();

    @Basic
    @Temporal(TemporalType.DATE)
    private java.util.Date createdDate;

    public Product() {
    }

    public Product(String title, String description, BigDecimal price, String color, String image, int quantity, boolean available, Brand brand, Collection<Country> countries, Collection<Tag> tags) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.color = color;
        this.image = image;
        this.quantity = quantity;
        this.available = available;
        this.brand = brand;
        this.countries = countries;
        this.tags = tags;
        this.createdDate = new Date();
    }
}
