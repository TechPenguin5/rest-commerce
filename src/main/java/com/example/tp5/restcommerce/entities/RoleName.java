package com.example.tp5.restcommerce.entities;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
