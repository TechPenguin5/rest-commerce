package com.example.tp5.restcommerce.entities;

import lombok.Data;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;


@Data
@Entity
@Table(name="WISHLIST")
public class Wishlist {

    private @Id
    @GeneratedValue
    Integer id;

    private boolean status;

    @ManyToMany
    private Collection<Product> products = new ArrayList<Product>();

//    @OneToOne
//    private User user;

    @Basic
    @Temporal(TemporalType.DATE)
    private java.util.Date addDate;

    public Wishlist() {
    }

    public Wishlist(boolean status, Collection<Product> products) {
        this.status = true;
        this.products = products;
        this.addDate = new Date();
    }
}