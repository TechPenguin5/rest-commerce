package com.example.tp5.restcommerce.entities;

import lombok.Data;
import javax.persistence.*;


@Data
@Entity
@Table(name="BRAND")
public class Brand {

    private @Id
    @GeneratedValue
    Integer id;

    private String name;

    public Brand() {
    }

    public Brand(String name) {
        this.name = name;
    }
}
