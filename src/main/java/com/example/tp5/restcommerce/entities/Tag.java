package com.example.tp5.restcommerce.entities;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@Entity
@Table(name="TAG")
public class Tag {

    private @Id
    @GeneratedValue
    Integer id;

    private String name;

    public Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }
}
