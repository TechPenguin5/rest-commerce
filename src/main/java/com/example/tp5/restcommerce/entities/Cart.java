package com.example.tp5.restcommerce.entities;

import lombok.Data;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;


@Data
@Entity
@Table(name="CART")
public class Cart {

    private @Id
    @GeneratedValue
    Integer id;

    private int quantity;
    private boolean status;

    @ManyToMany
    private Collection<Product> products = new ArrayList<Product>();

    @OneToOne
    private User user;

    @Basic
    @Temporal(TemporalType.DATE)
    private java.util.Date addDate;

    public Cart() {
    }

    public Cart(int quantity, Collection<Product> products, User user) {
        this.quantity = quantity;
        this.status = true;
        this.products = products;
        this.addDate = new Date();
        this.user = user;
    }
}
