package com.example.tp5.restcommerce.entities;

import lombok.Data;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Data
@Entity
public class User {

    private @Id
    @GeneratedValue
    Integer id;

    private String fName;
    private String lName;
    private String username;
    private String email;
    private String password;
    private boolean status;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    public User() {
    }

    public User(String fName, String lName, String username, String email, String password) {
        this.fName = fName;
        this.lName = lName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.status = true;
    }
}
