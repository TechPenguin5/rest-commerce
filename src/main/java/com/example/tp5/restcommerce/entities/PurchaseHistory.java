package com.example.tp5.restcommerce.entities;

import lombok.Data;
import javax.persistence.*;
import java.util.Date;


@Data
@Entity
public class PurchaseHistory {

    private @Id
    @GeneratedValue
    Integer id;

    private Integer productId;
    private Integer cartId;
    private Integer userId;

    @Basic
    @Temporal(TemporalType.DATE)
    private java.util.Date purchaseDate;

    public PurchaseHistory() {
    }

    public PurchaseHistory(Integer productId, Integer cartId, Integer userId, Date purchaseDate) {
        this.productId = productId;
        this.cartId = cartId;
        this.userId = userId;
        this.purchaseDate = purchaseDate;
    }
}
