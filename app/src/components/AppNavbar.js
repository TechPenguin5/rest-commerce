import React, {Component} from 'react';
import {Button, Form} from 'reactstrap';
import { Redirect } from 'react-router-dom';
import {IntlProvider, FormattedMessage} from 'react-intl';
import AuthService from './AuthService';


class AppNavbar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cart: [],
            isOpen: false,
        };
        this.toggle = this.toggle.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    handleChange(e){
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    }

    handleFormSubmit(e){
        e.preventDefault();

        this.Auth.login(this.state.username,this.state.password)
            .then(res =>{
                // this.props.history.replace('/');
            })
            .catch(err =>{
                alert(err);
            })
    }

    async componentDidMount() {

        fetch('/cart' , {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + this.props.token
            }
        }).then(response => response.json())
            .then(data => this.setState({
                cart: data,
                isOpen: false,
            }));

        return <Redirect to='/cart' />
    }

    handleLogout(){
        this.props.history.replace('/login');
    }

    search() {
        return <Redirect to='/products' />;
    }

    render() {

        const numCart = this.state.cart.length;
        const obj = this.state;

        return (
            <div>
                <header className="section-header">
                    <nav className="navbar navbar-top navbar-expand-lg">
                        <div className="container">
                            <button className="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#navbarsExample07" aria-controls="navbarsExample07"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"/>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarsExample07">
                                <ul className="navbar-nav mr-auto">
                                    <li><a href="#" className="nav-link"> <i className="fab fa-facebook"/> </a>
                                    </li>
                                    <li><a href="#" className="nav-link"> <i className="fab fa-instagram"/> </a>
                                    </li>
                                    <li><a href="#" className="nav-link"> <i className="fab fa-twitter"/> </a>
                                    </li>
                                </ul>
                                <ul className="navbar-nav">
                                    <li className="nav-item">
                                        <a href="/products" className="nav-link">
                                            <FormattedMessage id="Home.dayMessage" defaultMessage="Store"/>
                                    </a>
                                    </li>
                                    <li className="nav-item"><a href="/users" className="nav-link"> Users </a></li>
                                    <li className="nav-item"><a href="#" className="nav-link"> Help </a>
                                    </li>
                                    <li className="nav-item dropdown"><a href="#" className="nav-link dropdown-toggle" data-toggle="dropdown"> USD </a>
                                        <ul className="dropdown-menu dropdown-menu-right">
                                            <li><a id="sub-item" className="dropdown-item" href="#">EUR</a></li>
                                            <li><a id="sub-item" className="dropdown-item" href="#">GER</a></li>
                                            <li><a id="sub-item" className="dropdown-item" href="#">RUBL</a></li>
                                        </ul>
                                    </li>
                                    <li className="nav-item dropdown">
                                        <a href="#" className="nav-link dropdown-toggle" data-toggle="dropdown">Language</a>
                                        <ul className="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a id="sub-item" className="dropdown-item" href="?lang=en">English</a>
                                            </li>
                                            <li>
                                                <a id="sub-item" className="dropdown-item" href="?lang=fr">French</a>
                                            </li>
                                            <li>
                                                <a id="sub-item" className="dropdown-item" href="?lang=de">German</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <section className="header-main shadow-sm">
                        <div className="container">
                            <div className="row align-items-center">
                                <div className="col-lg-3 col-sm-4">
                                    <div className="brand-wrap">
                                        <img className="logo" src="images/logo.png"/>
                                        <a href="/" class="text-dark"><h2 className="logo-text">Rest-Commerce</h2></a>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-xl-5 col-sm-8">
                                    <Form onSubmit={this.search.bind(this)} className="search-wrap">
                                        <div className="input-group w-100">
                                            <input type="text" name="search" className="form-control" style={{width: '55%'}} placeholder="Search"/>
                                            <select className="custom-select" name="category">
                                                <option selected="selected" value="">All type</option>
                                                <option value="men">Men</option>
                                                <option value="women">Women</option>
                                                <option value="codex">Special</option>
                                                <option value="content">Latest</option>
                                            </select>
                                            <div className="input-group-append">
                                                <Button id="btn-search" className="btn btn-primary" type="submit">
                                                    <i className="fa fa-search"/>
                                                </Button>{' '}
                                            </div>
                                        </div>
                                    </Form>
                                </div>
                                <div className="col-lg-5 col-xl-4 col-sm-12">
                                    <div className="widgets-wrap float-right">
                                        <a href="/cart" className="widget-header mr-3">
                                            <div className="icontext">
                                                <div className="icon-wrap"><i
                                                    className="icon-sm round border fa fa-shopping-cart"/></div>
                                                <div className="text-wrap">
                                                    <span className="small badge badge-danger">{numCart}</span>
                                                    <div>Cart</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="/wishlist" className="widget-header mr-3">
                                            <div className="icontext">
                                                <div className="icon-wrap"><i
                                                    className="icon-sm round border fa fa-heart"/></div>
                                                <div className="text-wrap">
                                                    <small>Wish</small>
                                                    <div>List</div>
                                                </div>
                                            </div>
                                        </a>
                                        <div className="widget-header dropdown">
                                            <a href="#" data-toggle="dropdown" data-offset="20,10">
                                                <div className="icontext">
                                                    <div className="icon-wrap"><i
                                                        className="icon-sm round border fa fa-user"/></div>
                                                    <div className="text-wrap">
                                                        <small>Sign in | Join</small>
                                                        <div>My account <i className="fa fa-caret-down"/></div>
                                                    </div>
                                                </div>
                                            </a>
                                            <div className="dropdown-menu dropdown-menu-right">
                                                <form onSubmit={this.handleFormSubmit} className="px-4 py-3">
                                                    <div className="form-group">
                                                        <label>Email address</label>
                                                        <input type="text" name="username" className="form-control"
                                                               placeholder="email@example.com" onChange={this.handleChange}/>
                                                    </div>
                                                    <div className="form-group">
                                                        <label>Password</label>
                                                        <input type="password" name="password" className="form-control"
                                                               placeholder="Password" onChange={this.handleChange}/>
                                                    </div>
                                                    <input id="sign-in" value="Sign In" type="submit" className="btn"/>
                                                </form>
                                                <hr className="dropdown-divider"/>
                                                <a className="dropdown-item" href="/register">Need an account?</a>
                                                <a className="dropdown-item" href="#">Forgot password?</a>
                                                <form name="logoutForm" method="post">
                                                    <div>
                                                        <h2>Welcome {}</h2>
                                                    <p className="App-intro">
                                                        <button type="button" className="form-submit" onClick={this.handleLogout.bind(this)}>Logout</button>
                                                    </p>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </header>
            </div>
        );
    }
}

export default AppNavbar
