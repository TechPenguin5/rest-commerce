import React from 'react';
import Aux from '../hoc/auxiliary/Auxiliary';


const redirectToProductDetails = (id, history) => {
    history.push('/products/' + id);
}

const redirectToUpdateOwner = (id, history) => {
    history.push('/products/' + id);
}

const rediterctToDeleteOwner = (id, history) => {
    history.push('/products/' + id);
}

const product = (props) => {
    return (
        <Aux>
            <div onClick={() => redirectToProductDetails(props.product.id, props.history)} className="col-md-3" key={product.id}>
                <figure className="card card-sm card-product">
                    <div className="img-wrap product-image"><img src={props.product.image}/></div>
                    <figcaption className="info-wrap text-center">
                        <h6 className="title text-truncate product-title">{props.product.title}</h6>
                    </figcaption>
                </figure>
            </div>
        </Aux>
    )
}

export default product;