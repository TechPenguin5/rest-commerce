import React from 'react';

const internalServer = (props) => {
    return (
        <p className={'internalServer'}>{"500 SERVER ERROR, CONTACT ADMINISTRATOR!!!!"}</p>
    )
}

export default internalServer;