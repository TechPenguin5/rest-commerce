import React, {Component} from 'react';
import AppNavbar from '../components/AppNavbar';
import Spinner from '../components/Spinner';
import Footer from '../components/Footer'
import AuthService from '../components/AuthService';
import Aux from '../hoc/auxiliary/Auxiliary';
const Auth = new AuthService();

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {isLoading: true};
    }

    componentDidMount() {
        this.setState({isLoading: false});
    }

    render() {

        if (this.state.isLoading) {
            return (
                <Spinner/>
            );
        } else {
            return (
                <Aux>
                    <AppNavbar/>
                    {/*<div>*/}
                    {/*<div className="modal fade" id="incorrectmodal" tabIndex={-1} role="dialog"*/}
                    {/*aria-labelledby="exampleModalLongTitle" aria-hidden="true">*/}
                    {/*<div className="modal-dialog" role="document">*/}
                    {/*<div className="modal-content">*/}
                    {/*<div className="modal-header">*/}
                    {/*<h5 className="modal-title" id="ModalLongTitle">Please try again!</h5>*/}
                    {/*<button type="button" className="close" data-dismiss="modal" aria-label="Close">*/}
                    {/*<span aria-hidden="true">×</span>*/}
                    {/*</button>*/}
                    {/*</div>*/}
                    {/*<div className="modal-body">*/}
                    {/*Invalid username and password.*/}
                    {/*</div>*/}
                    {/*<div className="modal-footer">*/}
                    {/*<button type="button" className="btn btn-secondary" data-dismiss="modal">OK</button>*/}
                    {/*</div>*/}
                    {/*</div>*/}
                    {/*</div>*/}
                    {/*</div>*/}
                    {/*</div>*/}

                    {/*<div>*/}
                    {/*<div className="modal fade" id="logoutmodal" tabIndex={-1} role="dialog"*/}
                    {/*aria-labelledby="exampleModalLongTitle" aria-hidden="true">*/}
                    {/*<div className="modal-dialog" role="document">*/}
                    {/*<div className="modal-content">*/}
                    {/*<div className="modal-header">*/}
                    {/*<h5 className="modal-title" id="ModalLongTitle">Come back soon!</h5>*/}
                    {/*<button type="button" className="close clear-uri" data-dismiss="modal"*/}
                    {/*aria-label="Close">*/}
                    {/*<span aria-hidden="true">×</span>*/}
                    {/*</button>*/}
                    {/*</div>*/}
                    {/*<div className="modal-body">*/}
                    {/*You have successfully been signed out*/}
                    {/*</div>*/}
                    {/*<div className="modal-footer">*/}
                    {/*<button type="button" className="btn btn-secondary clear-uri"*/}
                    {/*data-dismiss="modal">OK*/}
                    {/*</button>*/}
                    {/*</div>*/}
                    {/*</div>*/}
                    {/*</div>*/}
                    {/*</div>*/}
                    {/*</div>*/}

                    <div id="carousel1_indicator" className="carousel slide" data-ride="carousel">
                        <ol className="carousel-indicators">
                            <li data-target="#carousel1_indicator" data-slide-to={0} className="active"/>
                            <li data-target="#carousel1_indicator" data-slide-to={1} className/>
                            <li data-target="#carousel1_indicator" data-slide-to={2} className/>
                        </ol>
                        <div className="carousel-inner">
                            <div className="carousel-item active">
                                <img className="d-block" src="images/slider/slide-05.jpg" alt="First slide"/>
                            </div>
                            <div className="carousel-item">
                                <img className="d-block" src="images/slider/slide-02.jpg" alt="Second slide"/>
                            </div>
                            <div className="carousel-item">
                                <img className="d-block" src="images/slider/slide-03.jpg" alt="Third slide"/>
                            </div>
                        </div>
                        <a className="carousel-control-prev" href="#carousel1_indicator" role="button"
                           data-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"/>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next" href="#carousel1_indicator" role="button"
                           data-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"/>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                    <Footer/>
                </Aux>
            );
        }
    }
}

export default Home;

