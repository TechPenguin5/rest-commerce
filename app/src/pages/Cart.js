import React, {Component} from 'react';
import {Button, Container, Form, FormGroup, Input, Label, ButtonGroup, Table} from 'reactstrap';
import AppNavbar from '../components/AppNavbar';
import {Link} from 'react-router-dom';
import Spinner from "../components/Spinner";

class Cart extends Component {

    product = {
    };

    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isLoading: true
        };

        this.remove = this.remove.bind(this);
    }

    async componentDidMount() {
        this.setState({isLoading: true});

        fetch('/cart' , {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + this.props.token
            }
        })
            .then(response => response.json())
            .then(data => this.setState({products: data, isLoading: false}));

    }

    async remove(id) {
        await fetch(`/cart/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.token
            }
        }).then(() => {
            let updatedProducts = [...this.state.products].filter(i => i.id !== id);
            this.setState({products: updatedProducts});
        });
    }

    render() {

        const {products, isLoading} = this.state;

        if (isLoading) {
            return(
                <Spinner/>
            );
        }

        var total = 0;
        for (var i = 0; i < this.state.products.length; i++) {
            total += this.state.products[i].price;
        }

        const cartList = products.map(product => {
            return (
                    <tr>
                        <td>
                            <figure className="media">
                                <div className="img-wrap"><img src={product.image} className="img-thumbnail img-sm" /></div>
                                <figcaption className="media-body">
                                    <Link to ={"/products/" + product.id} >
                                    <h6 className="title text-truncate">{product.title}</h6>
                                    </Link>
                                    <dl className="dlist-inline small">
                                        <dt>Size: </dt>
                                        <dd>XXL</dd>
                                    </dl>
                                    <dl className="dlist-inline small">
                                        <dt>Color: </dt>
                                        <dd>{product.color}</dd>
                                    </dl>
                                </figcaption>
                            </figure>
                        </td>
                        <td>
                            <select className="form-control">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </td>
                        <td>
                            <div className="price-wrap">
                                <var className="price">USD {product.price}</var>
                                <small className="text-muted">(${product.price} each)</small>
                            </div>
                        </td>
                        <td className="text-right">
                            <ButtonGroup>
                                <a data-original-title="Save to Wishlist" title href className="btn btn-outline-success" data-toggle="tooltip"> <i className="fa fa-heart" /></a>
                                <Button className="btn btn-outline-danger" onClick={() => this.remove(product.id)}> × Remove</Button>
                            </ButtonGroup>
                        </td>
                    </tr>
            );
        });

        if(this.state.products.length>0) {
            return (
                <div>
                    <AppNavbar/>
                    <Container fluid>
                        <div className="container container-push">
                            <div className="products-list">
                                <h3>Cart</h3>
                                <section className="section-content bg padding-y border-top">
                                    <div className="container">
                                        <div className="row">
                                            <main className="col-sm-9">
                                                <div className="card">
                                                    <table className="table table-hover shopping-cart-wrap">
                                                        <thead className="text-muted">
                                                        <tr>
                                                            <th scope="col">Product</th>
                                                            <th scope="col" width={120}>Quantity</th>
                                                            <th scope="col" width={120}>Price</th>
                                                            <th scope="col" className="text-right" width={200}>Action
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {cartList}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </main>
                                            <aside className="col-sm-3">
                                                <p className="alert alert-success">Add USD 5.00 of eligible items to
                                                    your order to qualify for FREE Shipping. </p>
                                                <dl className="dlist-align">
                                                    <dt>Total price:</dt>
                                                    <dd className="text-right">${total}</dd>
                                                </dl>
                                                <dl className="dlist-align">
                                                    <dt>Discount:</dt>
                                                    <dd className="text-right">-</dd>
                                                </dl>
                                                <dl className="dlist-align h4">
                                                    <dt>Total:</dt>
                                                    <dd className="text-right"><strong>USD {total}</strong></dd>
                                                </dl>
                                                <hr/>
                                                <figure className="itemside mb-3">
                                                    <aside className="aside"><img src="images/icons/pay-visa.png"/>
                                                    </aside>
                                                    <div className="text-wrap small text-muted">
                                                        Pay 84.78 AED ( Save 14.97 AED )
                                                        By using ADCB Cards
                                                    </div>
                                                </figure>
                                                <figure className="itemside mb-3">
                                                    <aside className="aside"><img
                                                        src="images/icons/pay-mastercard.png"/></aside>
                                                    <div className="text-wrap small text-muted">
                                                        Pay by MasterCard and Save 40%. <br/>
                                                        Lorem ipsum dolor
                                                    </div>
                                                </figure>
                                            </aside>
                                        </div>
                                    </div>
                                </section>

                            </div>
                        </div>
                    </Container>
                </div>
            );
        } else {
            return(
                <div>
                    <AppNavbar/>
                    <Container fluid>
                        <h3 class="text-center vh-center">Cart Empty</h3>
                    </Container>
                </div>
            );
        }
    }
}

export default Cart;