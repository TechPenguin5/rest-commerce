import React, {Component} from 'react';
import {Container} from 'reactstrap';
import AppNavbar from '../components/AppNavbar';
import Spinner from '../components/Spinner';
import {Link} from 'react-router-dom';
import Footer from "../components/Footer";
import * as repositoryActions from "../store/actions/RepositoryActions";
import {connect} from "react-redux";
import Product from '../components/Product';
import Aux from '../hoc/auxiliary/Auxiliary';


class ProductList extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const urlParam = this.props.location.search;
        let url = '/products';
        this.props.onGetData(url, {...this.props});
    }

    render() {
        let products = [];
        if (this.props.data && this.props.data.length > 0) {
            products = this.props.data.map((product) => {
                return (
                    <Product key={product.id} product={product} {...this.props} />
                )
            })
        } else {
            return (
                <Spinner/>
            )
        }

        return (
            <Aux>
                <AppNavbar/>
                <Container fluid>
                    <div className="container container-push">
                        <div className="products-list">
                            <h3>Products</h3>
                            <div id="code_prod_simple">
                                <div className="row">
                                    {products}
                                </div>
                            </div>
                        </div>
                    </div>
                </Container>
                <Footer/>
            </Aux>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        data: state.repository.data
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onGetData: (url, props) => dispatch(repositoryActions.getData(url, props))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductList);