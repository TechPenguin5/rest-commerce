import React, {Component} from 'react';
import {Button, Container, Form, FormGroup, Input, Label, ButtonGroup, Table} from 'reactstrap';
import AppNavbar from '../components/AppNavbar';

class Wishlist extends Component {

    product = {
    };

    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isLoading: true
        };

        this.remove = this.remove.bind(this);
    }

    async componentDidMount() {
        this.setState({isLoading: true});

        fetch('/wishlist' , {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + this.props.token
            }
        })
            .then(response => response.json())
            .then(data => this.setState({products: data, isLoading: false}));
    }

    async remove(id) {
        await fetch(`/wishlist/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            let updatedProducts = [...this.state.products].filter(i => i.id !== id);
            this.setState({products: updatedProducts});
        });
    }

    render() {

        const {products, isLoading} = this.state;

        if (isLoading) {
            return <p>Loading...</p>;
        }

        var total = 0;
        for (var i = 0; i < this.state.products.length; i++) {
            total += this.state.products[i].price;
        }

        const wishList = products.map(product => {
            return (
                    <tr>
                        <td>
                            <figure className="media">
                                <div className="img-wrap"><img src={product.image} className="img-thumbnail img-sm" /></div>
                                <figcaption className="media-body">
                                    <h6 className="title text-truncate">{product.title}</h6>
                                    <dl className="dlist-inline small">
                                        <dt>Size: </dt>
                                        <dd>XXL</dd>
                                    </dl>
                                    <dl className="dlist-inline small">
                                        <dt>Color: </dt>
                                        <dd>{product.color}</dd>
                                    </dl>
                                </figcaption>
                            </figure>
                        </td>
                        <td>
                            <div className="price-wrap">
                                <var className="price">USD {product.price}</var>
                                <small className="text-muted">(${product.price} each)</small>
                            </div>
                        </td>
                        <td className="text-right">
                            <ButtonGroup>
                                <a data-original-title="Save to Wishlist" title href className="btn btn-outline-success" data-toggle="tooltip"> <i className="fa fa-shopping-cart" /></a>
                                <Button className="btn btn-outline-danger" onClick={() => this.remove(product.id)}> × Remove</Button>
                            </ButtonGroup>
                        </td>
                    </tr>
            );
        });

        if(this.state.products.length>0) {
            return (
                <div>
                    <AppNavbar/>
                    <Container fluid>
                        <div className="container container-push">
                            <div className="products-list">
                                <h3>Wishlist</h3>
                                <section className="section-content bg padding-y border-top">
                                    <div className="container">
                                        <div className="row">
                                            <main className="col-sm-9">
                                                <div className="card">
                                                    <table className="table table-hover shopping-cart-wrap">
                                                        <thead className="text-muted">
                                                        <tr>
                                                            <th scope="col">Product</th>
                                                            <th scope="col">Price</th>
                                                            <th scope="col" className="text-right">Action
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {wishList}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </main>
                                            <aside className="col-sm-3">
                                            </aside>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </Container>
                </div>
            );
        } else {
            return(
                <div>
                    <AppNavbar/>
                    <Container fluid>
                        <h3 class="text-center vh-center">Wishlist Empty</h3>
                    </Container>
                </div>
            );
        }
    }
}

export default Wishlist;