import React, {Component} from 'react';
import {Button, Container, Form, FormGroup, Input, Label} from 'reactstrap';
import AppNavbar from '../components/AppNavbar';
import Footer from "../components/Footer";

class Product extends Component {

    product = {
    };

    brand = {
    };

    tags = {
    };

    countries = {
    };

    constructor(props) {
        super(props);
        this.state = {
            product: this.product,
            brand: this.brand,
            tags: this.tags,
            countries: this.countries

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleCart = this.handleCart.bind(this);
        this.handleWishlist = this.handleWishlist.bind(this);
    }

    async componentDidMount() {
        if (this.props.match.params.id !== 'new') {
            const product = await (await fetch(`/products/${this.props.match.params.id}`)).json();
            this.setState({product: product});

            const brand = product.brand;
            this.setState({brand: brand});

            const tags = product.tags;
            this.setState({tags: tags});

            const countries = product.countries;
            this.setState({countries: countries});
        }
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let product = {...this.state.product};
        product[name] = value;
        this.setState({product});
    }

    async handleCart (event) {
        event.preventDefault();
        const {product} = this.state;

        var userId = 0;

        await fetch('/cart', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Header': JSON.stringify(userId),
                'Authorization': 'Bearer ' + this.props.token
            },
            body: JSON.stringify(product),
        });
        this.props.history.push('/cart');
    }

    async handleWishlist(event) {
        event.preventDefault();
        const {product} = this.state;

        await fetch('/wishlist', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.token
            },
            body: JSON.stringify(product),
        });
        this.props.history.push('/wishlist');
    }


    render() {
        const {product} = this.state;
        const brand = this.state.brand;

        var x = "";
        for (var i = 0; i < this.state.countries.length; i++) {
             x += this.state.countries[i].name + ", ";
        }

        const countries = x;

        return <div>
            <AppNavbar/>
            <Container>
                <div className="interact-form container-push">
                    <a href="javascript:history.back()">Back</a>
                    <section id="product_detail">
                        <div id="code_prod_detail">
                            <div className="card">
                                <div className="row no-gutters">
                                    <aside className="col-sm-5">
                                        <article className="gallery-wrap">
                                            <div className="img-big-wrap">
                                                <div> <a href={product.image} data-fancybox><img src={product.image} /></a></div>
                                            </div>
                                        </article>
                                    </aside>
                                    <aside className="col-sm-7">
                                        <article className="p-5">
                                            <h3 className="title mb-3">{product.title}</h3>
                                            <div className="mb-3">
                                                <var className="price h3 text-warning">
                                                    <span className="currency">US $</span><span className="num">{product.price}</span>
                                                </var>
                                            </div>
                                            <dl>
                                                <dt>Description</dt>
                                                <dd><p>{product.description}</p></dd>
                                            </dl>
                                            <dl className="row">
                                                <dt className="col-sm-3">Brand</dt>
                                                <dd className="col-sm-9">{brand.name}</dd>
                                                <dt className="col-sm-3">Color</dt>
                                                <dd className="col-sm-9">{product.color}</dd>
                                                <dt className="col-sm-3">Ships to</dt>
                                                <dd className="col-sm-9">{countries}</dd>
                                            </dl>
                                            <div className="rating-wrap">
                                                <ul className="rating-stars">
                                                    <li style={{width: '80%'}} className="stars-active">
                                                        <i className="fa fa-star" /> <i className="fa fa-star" />
                                                        <i className="fa fa-star" /> <i className="fa fa-star" />
                                                        <i className="fa fa-star" />
                                                    </li>
                                                    <li>
                                                        <i className="fa fa-star" /> <i className="fa fa-star" />
                                                        <i className="fa fa-star" /> <i className="fa fa-star" />
                                                        <i className="fa fa-star" />
                                                    </li>
                                                </ul>
                                                <div className="label-rating">132 reviews</div>
                                                <div className="label-rating">154 orders </div>
                                            </div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-sm-5">
                                                    <dl className="dlist-inline">
                                                        <dt>Quantity: </dt>
                                                        <dd>
                                                            <select className="form-control form-control-sm" style={{width: 70}}>
                                                                <option> 1 </option>
                                                                <option> 2 </option>
                                                                <option> 3 </option>
                                                            </select>
                                                        </dd>
                                                    </dl>
                                                </div>
                                                <div className="col-sm-7">
                                                    <dl className="dlist-inline">
                                                        <dt>Size: </dt>
                                                        <dd>
                                                            <label className="form-check form-check-inline">
                                                                <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" defaultValue="option2" />
                                                                <span className="form-check-label">SM</span>
                                                            </label>
                                                            <label className="form-check form-check-inline">
                                                                <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" defaultValue="option2" />
                                                                <span className="form-check-label">MD</span>
                                                            </label>
                                                            <label className="form-check form-check-inline">
                                                                <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" defaultValue="option2" />
                                                                <span className="form-check-label">XXL</span>
                                                            </label>
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <hr />

                                            <Form onSubmit={this.handleCart}>
                                                <FormGroup>
                                                    <Button className="btn  btn-outline-primary" type="submit"><i className="fas fa-shopping-cart" /> Add to Cart </Button>{' '}
                                                </FormGroup>
                                            </Form>

                                            <Form onSubmit={this.handleWishlist}>
                                                <FormGroup>
                                                    <Button className="btn  btn-outline-secondary" type="submit"><i className="fas fa-shopping-cart" /> Add to Wishlist </Button>{' '}
                                                    <a href="#" className="btn  btn-primary"> Buy now </a>
                                                </FormGroup>
                                            </Form>

                                        </article>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </Container>
            <Footer/>
        </div>
    }
}

export default Product;