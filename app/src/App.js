import React, { Component } from 'react';


class App extends Component {

    handleLogout(){
        this.props.history.replace('/login');
    }

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <h2>Welcome {this.props.user.sub}</h2>
                </div>
                <p className="App-intro">
                    <button type="button" className="form-submit" onClick={this.handleLogout.bind(this)}>Logout</button>
                </p>
            </div>
        );
    }
}

export default App;