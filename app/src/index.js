import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Login from './components/Login';
import ProductDetails from "./pages/ProductDetails";
import UserEdit from "./pages/UserEdit";
import Cart from "./pages/Cart";
import Wishlist from "./pages/Wishlist";
import UserList from "./pages/UserList";
import Home from "./pages/Home";
import Navbar from "./components/AppNavbar";
import repositoryReducer from './store/reducers/RepositoryReducer';
import errorHandlerReducer from './store/reducers/ErrorHandlerReducer';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import asyncComponent from './hoc/async/AsyncComponent';
import InternalServer from './pages/error/InternalServer';
import Aux from './hoc/auxiliary/Auxiliary';
import {IntlProvider, FormattedMessage} from 'react-intl';

const AsyncOwnerList = asyncComponent(() => {
    return import('./pages/ProductList');
});

const rootReducers = combineReducers({
    repository: repositoryReducer,
    errorHandler: errorHandlerReducer
})

const store = createStore(rootReducers, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <IntlProvider>
                <Aux>
                    <Route path='/' exact={true} component={Home}/>
                    <Route exact path="/login" component={Login} />
                    <Route path='/users' exact={true} component={UserList}/>
                    <Route path='/products' exact={true} component={AsyncOwnerList}/>
                    <Route path='/products/:id' component={ProductDetails}/>
                    <Route path='/cart' component={Cart}/>
                    <Route path='/wishlist' component={Wishlist}/>
                    <Route path='/users/:id' component={UserEdit}/>
                    <Route path='/navbar' component={Navbar}/>
                    <Route path="/500" component={InternalServer} />
                </Aux>
            </IntlProvider>
        </Router>
    </Provider>,
    document.getElementById('root')
);


serviceWorker.register();
