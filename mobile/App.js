import React, {Component} from 'react';
import {TouchableOpacity,ScrollView,View,ActivityIndicator,Text,Alert,Image} from 'react-native';

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {products: []};
    }

    componentDidMount() {

        fetch('http://10.3.5.197:8080/products')
            .then(response => response.json())
            .then(data => this.setState({products: data}));
    }

    render() {

	    const products = this.state.products;

	    const productsList = products.map(product => {return (

	    	<View style={{paddingTop: 10,paddingLeft: 0}} key={product.id}> 
	    		<Image style = {{ width: 200, height: 200 }} source={product.image}/>
		 		<Text style = {{paddingTop: 10,paddingLeft: 10,color: '#666'}}>{product.title}</Text>
	 		</View>
	    )});


        return (
        	<ScrollView style = {{paddingTop: 10,paddingLeft: 0}}>
				{productsList}
			</ScrollView>
		);
    }
}